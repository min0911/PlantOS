#include <stdio.h>
#include <syscall.h>
void copy_drive(char d, char *p1, char *p2) {
  char *buf = malloc(strlen(p2) + 4);
  if (p2[0] == '/') {
    sprintf(buf, "%c:%s", d, p2);
  } else {
    sprintf(buf, "%c:\\%s",d, p2);
  }
  Copy(p1, buf);
  free(buf);
}
void change_disk(char d) {
  char *buf = malloc(4);
  sprintf(buf, "rdrv %c", d);
  system(buf);
  free(buf);
}
int main() {
  char d, c;
  c = api_current_drive();
R:
  printf("Which drive do you want to install? [A/C/D/E/F]\n");
  d = getch();
  d = toupper(d);
  printf("nasm will install in Drive %c [y/n]\n", d);
  if (getch() == 'n') {
    goto R;
  }
  printf("Reading nasm001.bin file...");
  int fsz1 = filesize("nasm001.bin");
  char *buf1 = malloc(fsz1);
  api_ReadFile("nasm001.bin", buf1);
  printf("done.\n");
  printf("Please insert nasm2 disk and press enter to continue...");
R2:
  while (1) if (getch() == '\n') break;
  printf("\n");
  change_disk(c);
  int fsz2 = filesize("nasm002.bin");
  if (fsz2 == -1) {
	printf("Insert a wrong disk, retry...");
	goto R2;
  }
  printf("Reading nasm002.bin file...");
  char *whole_file = malloc(fsz1 + fsz2);
  api_ReadFile("nasm002.bin", whole_file + fsz1);
  memcpy((void *)whole_file, (void *)buf1, fsz1);
  free((void *)buf1);
  printf("done.\n");
  printf("Merging and copying nasm.bin...");
  change_disk(d);
  Edit_File("nasm.bin", whole_file, fsz1 + fsz2, 0);
  free((void *)whole_file);
  printf("done.\n");
  printf("Please insert ndisasm disk and press enter to continue...");
R3:
  while (1) if (getch() == '\n') break;
  printf("\n");
  change_disk(c);
  int fsz3 = filesize("ndisasm.bin");
  if (fsz3 == -1) {
	printf("Insert a wrong disk, retry...");
	goto R3;
  }
  printf("Copying ndisasm.bin...");
  copy_drive(d, "ndisasm.bin", "/ndisasm.bin");
  printf("done.\n\n");
  return;
}