#include <dos.h>
int init_ok_flag = 0;
extern unsigned int PCI_ADDR_BASE;
char *shell_data;
void idle() {
  while (1) {
  }
}
void init() {
  logk("init task has started!\n");
  PCI_ADDR_BASE = (unsigned int)page_malloc(1 * 1024 * 1024);
  init_PCI(PCI_ADDR_BASE);
  current_task()->alloc_addr = page_malloc(512 * 1024);
  current_task()->alloc_size = 512 * 1024;
  logk("memory init.\n");
  current_task()->mm =
      memory_init(current_task()->alloc_addr, current_task()->alloc_size);
  ide_initialize(0x1F0, 0x3F6, 0x170, 0x376, 0x000);
  init_palette();
  vfs_mount_disk(current_task()->drive, current_task()->drive);
  vfs_change_disk(current_task()->drive);
  env_init();
  init_networkCTL();
  init_network();

  if (env_read("network") == NULL) {
    printk("would you like to enable network?(y/n)\n");
    switch (getch()) {
    case 'y':
    case 'Y':
      env_write("network", "enable");
      break;
    default:
      env_write("network", "disable");
      break;
    }
  }
  logk("%s\n", env_read("network"));
  if (strcmp(env_read("network"), "enable") == 0) {
    logk("init card\n");
    init_card();
    // for(;;);
  }
  running_mode = POWERINTDOS;
  //SwitchToHighTextMode();
  FILE *fp = fopen("/other/font.bin", "r");
  ascfont = fp->buffer;
  fp = fopen("/other/hzk16", "r");
  hzkfont = fp->buffer;
  init_ok_flag = 1;
  extern struct tty *tty_default;
  tty_set(current_task(), tty_default);
  clear();

  shell_data = (char *)page_malloc(vfs_filesize("psh.bin"));
  vfs_readfile("psh.bin", shell_data);
  os_execute_no_ret("psh.bin", "psh.bin");
  page_free(current_task()->top - 64 * 1024, 64 * 1024);
  page_free(current_task()->nfs, sizeof(vfs_t));
  task_kill(current_task()->tid);
  for (;;)
    ;
}