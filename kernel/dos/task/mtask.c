// 多任务重构 -- mtask.c (区别与以前的多任务)
#include <dos.h>
void free_pde(unsigned addr);
char default_drive, default_drive_number;
static char flags_once = false;
mtask m[255];
struct TSS32 tss;
mtask *idle_task;
mtask *current = NULL;
unsigned get_cr3() { asm volatile("movl %cr3, %eax\n"); }
void set_cr3(uint32_t pde) { asm volatile("movl %%eax, %%cr3\n" ::"a"(pde)); }
static void init_task() {
  for (int i = 0; i < 255; i++) {
    m[i].jiffies = 0;
    m[i].user_mode = 0;
    m[i].running = 0;
    m[i].timeout = 0;
    m[i].state = 0; // EMPTY
    m[i].tid = i;
    m[i].ptid = -1;
    m[i].keyboard_press = NULL;
    m[i].keyboard_release = NULL;
    m[i].urgent = 0;
    m[i].fpu = NULL;
    m[i].fpu_flag = 0;
    m[i].fifosleep = 0;
    m[i].mx = 0;
    m[i].my = 0;
    m[i].line = NULL;
    m[i].timer = NULL;
    m[i].nfs = NULL;
    m[i].mm = NULL;
  }
}
void task_next() {
  if (current->running < current->timeout - 1 && current->state == 1) {
    current->running++;
    return; // 不需要调度，当前时间片仍然属于你
  }
  current->running = 0;
  current->jiffies = global_time;
  mtask *next = NULL;
  for (int i = current->tid; i < 255; i++) {
    mtask *p = (&(m[i]));
    if (p == current) {
      continue;
    }
    if (!(p->running <= p->timeout - 1)) {
      continue;
    }
    if (p->state != 1) // RUNNING
    {
      continue;
    }
    next = p;
    break;
  }
  if (next == NULL) {
    next = idle_task;
  }
  if (next->user_mode) {
    tss.esp0 = next->top;
  }
  fpu_disable();
  task_switch(next);
}

mtask *create_task(unsigned eip, unsigned esp, unsigned ticks, unsigned floor) {
  mtask *t = NULL;
  for (int i = 0; i < 255; i++) {
    if (m[i].state == 0) {
      t = &(m[i]);
      break;
    }
  }
  if (!t) {
    return NULL;
  }
  io_cli();
  t->esp = esp - sizeof(stack_frame);
  t->esp->eip = eip;
  t->user_mode = 0;
  if (current == NULL) {
    t->pde = PDE_ADDRESS;
  } else {
    t->pde = pde_clone(current_task()->pde);
  }
  t->top = esp;
  t->floor = floor;
  t->running = 0;
  t->timeout = ticks;
  t->state = 1; // running
  t->drive_number = default_drive_number;
  t->drive = default_drive;
  if (!flags_once) {
    if (memcmp((void *)"FAT12   ", (void *)0x7c00 + BS_FileSysType, 8) == 0 ||
        memcmp((void *)"FAT16   ", (void *)0x7c00 + BS_FileSysType, 8) == 0) {
      if (*(unsigned char *)(0x7c00 + BS_DrvNum) >= 0x80) {
        default_drive_number =
            *(unsigned char *)(0x7c00 + BS_DrvNum) - 0x80 + 0x02;
      } else {
        default_drive_number = *(unsigned char *)(0x7c00 + BS_DrvNum);
      }
    } else if (memcmp((void *)"FAT32   ",
                      (void *)0x7c00 + BPB_Fat32ExtByts + BS_FileSysType,
                      8) == 0) {
      if (*(unsigned char *)(0x7c00 + BPB_Fat32ExtByts + BS_DrvNum) >= 0x80) {
        default_drive_number =
            *(unsigned char *)(0x7c00 + BPB_Fat32ExtByts + BS_DrvNum) - 0x80 +
            0x02;
      } else {
        default_drive_number =
            *(unsigned char *)(0x7c00 + BPB_Fat32ExtByts + BS_DrvNum);
      }
    } else {
      if (*(unsigned char *)(0x7c00) >= 0x80) {
        default_drive_number = *(unsigned char *)(0x7c00) - 0x80 + 0x02;
      } else {
        default_drive_number = *(unsigned char *)(0x7c00);
      }
    }
    default_drive = default_drive_number + 0x41;
    flags_once = true;
  }
  extern int init_ok_flag;
  if (init_ok_flag) {
    vfs_change_disk_for_task(t->drive, t);
  }
  io_sti();
  return t;
}
mtask *get_task(unsigned tid) {
  if (tid >= 255) {
    return NULL;
  }
  if (m[tid].state == 4) {
    return NULL;
  }
  return &(m[tid]);
}
void task_to_user_mode(unsigned eip, unsigned esp) {

  unsigned addr = (unsigned)current->top;

  addr -= sizeof(intr_frame_t);
  intr_frame_t *iframe = (intr_frame_t *)(addr);

  iframe->edi = 1;
  iframe->esi = 2;
  iframe->ebp = 3;
  iframe->esp_dummy = 4;
  iframe->ebx = 5;
  iframe->edx = 6;
  iframe->ecx = 7;
  iframe->eax = 8;

  iframe->gs = 0;
  iframe->ds = GET_SEL(3 * 8, SA_RPL3);
  iframe->es = GET_SEL(3 * 8, SA_RPL3);
  iframe->fs = GET_SEL(3 * 8, SA_RPL3);
  iframe->ss = GET_SEL(3 * 8, SA_RPL3);
  iframe->cs = GET_SEL(4 * 8, SA_RPL3);
  iframe->eip = eip;
  iframe->eflags = (0 << 12 | 0b10 | 1 << 9);
  iframe->esp = esp; // 设置用户态堆栈
  current->user_mode = 1;
  tss.esp0 = current->top;
  change_page_task_id(current_task()->tid, iframe->esp - 64 * 1024, 64 * 1024);
  io_sti();
  asm volatile("movl %0, %%esp\n"
               "popa\n"
               "pop %%gs\n"
               "pop %%fs\n"
               "pop %%es\n"
               "pop %%ds\n"
               "iret" ::"m"(iframe));
  for (;;)
    ;
}

void task_kill(unsigned tid) {
  for (int i = 0; i < 255; i++) {
    if (m[i].state == 0)
      continue;
    if (m[i].tid == tid)
      continue;
    if (m[i].ptid == tid) {
      task_kill(m[i].tid);
    }
  }

  free_pde(m[tid].pde);
  gc(tid); // 释放内存
  if (m[tid].fpu) {
    free(m[tid].fpu);
  }
  m[tid].state = 0;
  m[tid].urgent = 0;
  m[tid].fpu = NULL;
  m[tid].fpu_flag = 0;
  m[tid].fifosleep = 0;
  m[tid].mx = 0;
  m[tid].my = 0;
  m[tid].line = NULL;
  m[tid].jiffies = 0;
  m[tid].timer = NULL;
  m[tid].ptid = -1;
  m[tid].nfs = NULL;
  m[tid].mm = NULL;
}

mtask *current_task() { return current; }
int into_mtask() {
  init_task();
  struct SEGMENT_DESCRIPTOR *gdt = (struct SEGMENT_DESCRIPTOR *)ADR_GDT;
  memset(&tss, 0, sizeof(tss));
  tss.ss0 = 1 * 8;
  set_segmdesc(gdt + 103, 103, &tss, AR_TSS32);
  load_tr(103 * 8);
  idle_task = create_task(idle, page_malloc(64 * 1024) + 64 * 1024, 1, 3);
  create_task(init, page_malloc(64 * 1024) + 64 * 1024, 5, 1);
  init_float();
  task_start(&(m[0]));
}
void task_set_fifo(mtask *task, struct FIFO8 *kfifo, struct FIFO8 *mfifo) {
  task->keyfifo = kfifo;
  task->mousefifo = mfifo;
}
struct FIFO8 *task_get_key_fifo(mtask *task) {
  return task->keyfifo;
}
void task_sleep(mtask *task) {
  task->state = 3;
  task->fifosleep = 1;
}
void task_wake_up(mtask *task) {
  task->state = 1;
  task->fifosleep = 0;
}
void task_run(mtask *task) {
  // 加急一下
  task->urgent = 1;
}
void task_fifo_sleep(mtask *task) { task->fifosleep = 1; }
struct FIFO8 *task_get_mouse_fifo(mtask *task) {
  return task->mousefifo;
}
void task_lock() {
  if (current_task()->ptid == -1) {
    for (int i = 0; i < 255; i++) {
      if (m[i].state == 0)
        continue;
      if (m[i].tid == get_tid(current_task()))
        continue;
      if (m[i].ptid == get_tid(current_task()) && m[i].state == 1) {
        m[i].state = 2; // WAITING
      }
    }
  } else {
    for (int i = 0; i < 255; i++) {
      if (m[i].state == 0)
        continue;
      if (m[i].tid == get_tid(current_task()))
        continue;
      if ((m[i].tid == current_task()->ptid ||
           m[i].ptid == current_task()->ptid) &&
          m[i].state == 1) {
        m[i].state = 2; // WAITING
      }
    }
  }
}
void task_unlock() {
  if (current_task()->ptid == -1) {
    for (int i = 0; i < 255; i++) {
      if (m[i].state == 0)
        continue;
      if (m[i].tid == get_tid(current_task()))
        continue;
      if (m[i].ptid == get_tid(current_task()) && m[i].state == 2) {
        m[i].state = 1; // RUNNING
      }
    }
  } else {
    for (int i = 0; i < 255; i++) {
      if (m[i].state == 0)
        continue;
      if (m[i].tid == get_tid(current_task()))
        continue;
      if ((m[i].tid == current_task()->ptid ||
           m[i].ptid == current_task()->ptid) &&
          m[i].state == 2) {
        m[i].state = 1; // RUNNING
      }
    }
  }
}
uint32_t get_father_tid(mtask *t) {
  if (t->ptid == -1) {
    return get_tid(t);
  }
  return get_father_tid(get_task(t->ptid));
}